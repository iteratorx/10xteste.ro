const extract = require('./extract')

const months = ['ianuarie', 'februarie', 'martie', 'aprilie', 'mai', 'iunie', 'iulie', 'august', 'septembrie', 'octombrie', 'noiembrie', 'decembrie']
const stringMonths = months.join("|")
console.log(stringMonths)
const searchExpressions = {
  today: [
    // /Pânăastăzi([0-9a-z]*)peteritoriulRomâniei/,
    // /Pânăastăzi([0-9a-z]*)lanivel/,
    new RegExp(`stăzi([0-9]{1,2}(?:${stringMonths}))`)
  ],
  newCases: [
    // /rate alte ([0-9/.]*) de noi cazuri/,
    /aufostconfirmate([0-9]+)(?:de)?cazurinoi/,
    /aufostconfirmate([0-9]+)cazurinoi/,
    /aufostînregistratealte([0-9]+)(?:de)?noicazurideîmbolnăvire/
  ],
  totalCases: [
    /peteritoriulRomânieiaufostconfirmate([0-9]*)(?:de)?cazuridepersoaneinfectate/,
    /peteritoriulRomânieiaufostconfirmate([0-9]*)(?:de)?cazuridecetățeniinfectați/,
  ],

  recoveredTotalCases: [/confirmatepozitiv([0-9]*)aufostdeclaratevindecateșiexternate/],
  totalTests: [
    /Pânălaaceastădatălanivelnaționalaufostprelucrate([0-9]*)/,

  ],
  negativeTests: [/Dintreacestea([0-9]*)aufostcurezulta/],

  atiCasesNow: /LaATIînacestmomentsuntinternați([0-9]*)(?:de)?pacienți/,
  quarantineNow: /încarantinăinstituționalizatăsunt([0-9]*)(?:de)?persoane/,
  selfIsolationNow: [
    /Alte([0-9]*)(?:de)?persoanesuntînizolareladomiciliu/,

  ],
  finesLastDay: /aufostaplicatesancţiunicontravenţionaleînvaloarede([0-9/.]*)(?:de)?lei/,
  forcedQuarantineTotal: [
    /carantinăinstituționalizată([0-9]*)(?:de)?persoanecarenuaurespectat/
  ],
  circulationViolationsToday: [
    /ore([0-9]*)(?:de)?persoanecarenuaurespectatmăsuraprivindrestricţionareacirculaţiei/
  ],
  deathsTotal: [
    /pânăacum([0-9]*)(?:de)?persoanediagnosticatecuinfecțiecuCOVID-19internateînspitale(?:.*)audecedat/
  ]
}

const urls = [

  // 'https://www.mai.gov.ro/buletin-informativ-01-martie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/buletin-informativ-02-martie-2020-ora-10-00/',
  // 'https://www.mai.gov.ro/buletin-informativ-03-martie-2020-ora-16-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-ora-17-00/',
  // 'https://www.mai.gov.ro/buletin-de-presa-5-martie-ora-16-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-6-martie-2020-ora-16-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-7-martie-ora-17-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-8-martie-ora-12-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-9-martie-ora-16-00/',

  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-10-martie-ora-16-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-11-martie-ora-16-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-12-martie-ora-16-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-13-martie-ora-16-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-14-martie-ora-16-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-15-martie-ora-17-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-16-martie-ora-18-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-17-martie-ora-18-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-18-martie-ora-18-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-19-martie-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-20-martie-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-21-martie-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-22-martie-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-23-martie-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-24-martie-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-25-martie-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-26-martie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-27-martie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-28-martie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-29-martie-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-30-martie-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-30-martie-ora-13-00-2/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-1-aprilie-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-2-aprilie-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-3-aprilie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-4-aprilie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-5-aprilie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-6-aprilie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-6-aprilie-2020-ora-13-00-2/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-8-aprilie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-9-aprilie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-10-aprilie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-11-aprilie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-12-aprilie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-13-aprilie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-14-aprilie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-15-aprilie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-16-aprilie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-16-aprilie-2020-ora-13-00-2/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-18-aprilie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-19-aprilie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-20-aprilie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-21-aprilie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-22-aprilie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-23-aprilie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-24-aprilie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-25-aprilie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-26-aprilie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-27-aprilie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-28-aprilie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-29-aprilie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-30-aprilie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/21331-2/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-2-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-3-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-4-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-5-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-6-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-7-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/21402-2/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-9-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-10-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-11-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-12-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-13-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-14-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-15-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-16-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-17-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-18-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-19-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-20-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-21-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-22-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-23-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-24-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-25-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-26-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-27-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-28-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-29-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-30-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-31-mai-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-1-iunie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-2-iunie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-3-iunie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-3-iunie-2020-ora-13-00-2/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-5-iunie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-6-iunie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-7-iunie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-8-iunie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-9-iunie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-10-iunie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-11-iunie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-12-iunie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-13-iunie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-14-iunie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-15-iunie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-16-iunie-2020-ora-13-00/',
  // 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-17-iunie-2020-ora-13-00/',
  'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-18-iunie-2020-ora-13-00/',

];

(async function doExtraction() {
  const result = await extract.extractPattersFromUrls(searchExpressions, urls)
  //   const result = await extract.work(urls)




  const fs = require('fs');


  let rawData = fs.readFileSync("data/seed.json")
  const seedData = JSON.parse(rawData)

  let finalData = Object.assign(seedData, result)
  // let finalData = Object.assign({}, result)


  let data = JSON.stringify(finalData, null, 2)
  console.log(data)
  fs.writeFileSync('data/stats.json', data);
})()


// console.log(stringMonths)

// const text = 'Astăzi22martieaufostînregistrateprimele2decesepeteritoriulRomânieialeunorpacienţiinfectaţicunoultipdecoronavirusEstevorbadesprepacientulînvârstăde67deaniconfirmatcuCOVID-19pe18032020laUPUdincadrulSpitaluluiFiliașișitransferatînaceeașidatălaSCBICraiovapacientulaveaafecțiunipreexistentefoartegravepatologieneoplazicăînstadiuterminal(cancer)Aldoileapacientînvârstăde74deanierainternatlaSpitalulClinicdeBoliInfecţioaseşiTropicaleDrVictorBabeş–BucureștitransferatdelaSpitalulUniversitardeUrgenţăBucureştiavândobo'


// const e = new RegExp(`Astăzi([0-9]{1,2}(?:${stringMonths}))`)


// const res = text.match(e)

// console.log(res)