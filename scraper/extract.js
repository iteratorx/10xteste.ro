const axios = require('axios')
const cheerio = require("cheerio")


async function work(urls) {
  const requests = urls.map(async url => {
    // console.log(url)
    const res = await    axios.get(url)
    // console.log(res.data)
    const $ = cheerio.load(res.data)

  // console.log($(".entry-content").first().text())
  });


  return Promise.all(requests)
}


function findFirstMatch(text, expressions) {
  const targets = [].concat(expressions)

  console.log(targets)
  for (let index in targets) {
    let expression = targets[index]
    // console.log('testing expression', expression)
    let found = text.match(expression)
    // console.log(found)
    if (found && found.length > 1) {
      let value = found[1].split(".").join("")
      // console.log('found value')
      return value;
    }
    // console.log("not found", expression)

  }
  return null
}

async function extractPattersFromUrls(searches, urls) {

  const resultsForAllUrls = {}

  for (url of urls) {
    console.log(url)

    let res;
    try {
      res = await axios.get(url)
    } catch (error) {
      // console.log(error)
      console.log("error")
      continue

    }

    // let text = cleanText(res.data)
    // console.log("clean text:", text)


    const $ = cheerio.load(res.data)

    let text = cleanText($(".entry-content").first().text())
    console.log(text)

    let results = {
      ...extractPattersFromText(searches, text),
      "url": url
    }


    resultsForAllUrls[results.today] = results
  }

  return resultsForAllUrls
}


function cleanText(text) {
  return text.replace(/[ \,\.\n\s]/g, "")
}

function extractPattersFromText(searches, text) {

  let results = {}
  Object.entries(searches).forEach(([variable, expressions]) => {
    // console.log(expressions)
    const firstMatch = findFirstMatch(text, expressions)
    results[variable] = firstMatch
  }
  )

  return results

}

exports.cleanText = cleanText;
exports.extractPattersFromText = extractPattersFromText
exports.findFirstMatch = findFirstMatch
exports.extractPattersFromUrls = extractPattersFromUrls
exports.work = work