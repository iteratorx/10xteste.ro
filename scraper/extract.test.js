const extract = require('./extract')

test("extracts 23000 out of 'ddd 23.000 ddd'", () => {
  expect(extract.findFirstMatch('ddd 23.000 ddd', /d ([0-9/.]*) d/)).toBe("23000")
})

test("extracts 23 out of 'ddd 23 ddd'", () => {
  expect(extract.findFirstMatch('ddd 23 ddd', /d ([0-9/.]*) d/)).toBe("23")
})


test("extracts 23000 out of array ['ddx 23.000 ddx','ddd 23.000 ddd']", () => {
  expect(extract.findFirstMatch('ddd 23.000 ddd', /d ([0-9/.]*) d/)).toBe("23000")
})


test("extracts 10 out of 'au fost confirmate 10 cazuri noi'", () => {
  expect(extract.findFirstMatch('au fost confirmate 10 cazuri noi', /au fost confirmate ([0-9/.]*) cazuri noi/)).toBe("10")
})

test('handles one nbsp', () => {
  expect(extract.extractPattersFromText({
    search: [/Dintre acestea, ([0-9/.]*) au fost cu rezultat/]
  }, extract.cleanText("Dintre acestea, 3.540&nbsp; au fost cu rezultat negativ"))).toStrictEqual({
    "search": "3540"
  })
})

test('escape regexp', () => {
  expect("Dintre acestea, 3.540&nbsp; au fost cu rezultat negativ".replace(/\&nbsp;/g, "")).toStrictEqual(
    "Dintre acestea, 3.540 au fost cu rezultat negativ"
  )
})

test("clean text", () => {
  expect(extract.cleanText("Dintre acestea, 3.540&nbsp; au fost cu rezultat negativ")).toStrictEqual("Dintre acestea, 3.540 au fost cu rezultat negativ")
})