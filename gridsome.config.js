// This is where project configuration and plugin options are located. 
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
  siteName: 'De zece ori mai multe teste CoVid19',
  plugins: [
    //   {
    //   use: 'gridsome-source-google-docs',
    //   options: {
    //     foldersIds: ['a8o3d384gdjbvxlfdi8rsz3'],
    //     clientId: '709160772412-p4anccq5qgob5hm8r9kg48ek2456u52q.apps.googleusercontent.com',
    //     clientSecret: 'fiyHxu5MISQkvg4yoB986ekF',
    //     apiKey: 'AIzaSyA-F_WWmZwa8GqE33uW2PFWT6SvPfdCJBM',
    //   },
    // }
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'content/**/*.md',
        typeName: 'Post',
        remark: {
          // remark options
        }
      }
    },
    {
      use: 'gridsome-plugin-plausible'
    },
    {
      use: 'gridsome-plugin-fathom',
      options: {
        siteId: 'QTZUHBOG',
        // usefal if you're running a self-hosted fathom instance
        // trackerUrl: 'your-custom-url',
        // declare this to ensure your tracking only occurs on a single host
        host: '10xteste.ro',
        // set to true for local debugging; defaults to false
        debug: false
      }
    }
  ],
  transformers: {
    remark: {
      // global remark options
    }
  }
}
