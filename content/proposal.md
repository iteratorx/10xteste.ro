# O metodă de creștere masivă a numărului de teste COVID-19 în România

![Împreună putem face de 10x mai multe teste COVID-19](/poster10xteste.jpeg "De zece ori mai multe teste")

## Expunere

Testarea COVID-19 presupune 3 etape:

1. Recoltarea probelor biologice
2. Extracția ARN din probele recoltate
3. Testarea ARN prin proces RT-qPCR

În prezent, probele sunt recoltate în diferite centre de recoltare, de unde sunt trimise la laboratoarele care dețin echipamente RT-qPCR.

![Testarea în prezent](/descriere-1.svg "Testarea COVID-19 în prezent")

În momentul de față, etapa de extracție ARN (pct 2) limitează cel mai mult viteza de testare, întrucât este un proces în mare parte manual, care consumă mult timp.

Pentru pct.3 (testarea ARN prin proces RT-qPCR), aparatura este costisitoare și nu toate laboratoarele o au în dotare. Însă procesul propriu-zis este rapid: se pot procesa câte 96 de probe în aproximativ 2 ore.

Astfel, dacă extracția ARN ar fi făcută în mult mai multe locații și ARN-ul extras ar fi transportat apoi la centrele dotate cu aparate RT-qPCR, am putea efectua mult mai multe teste cu aceleași mașini RT-qPCR.

Pentru extracția ARN sunt necesare:

1. Kit extracție ARN Viral
2. Centrifugă
3. Vortex
4. Spinner
5. Dry bath
6. Pipete automate

În momentul de față majoritatea laboratoarelor private de analize medicale au echipamentul necesar și experiența pentru a face procesul de Extracție ARN din probele recoltate.
Acest proces nu necesita studii avansate sau cunoștințe de folosire echipamente speciale, ci reprezintă o serie de pași ce trebuie urmați și pot fi duși la capăt de oricine cu minime abilitați de lucrat în laboratoare cum ar fi studenți sau doctoranzi.

## Propunere

Propunem ca pasul 2.(extracție ARN) să se poată executa și în laboratoare medicale intermediare, după cum urmează:

1. O parte din probele recoltate vor fi prelucrate intermediar în diverse laboratoare medicale intermediare, publice sau private, care vor efectua DOAR extracța ARN
2. ARN-ul extras la prelucrearea intermediară va fi trimis la centrele unde se face RT-qPCR (centrele actuale).

Scopul este să utilizăm la maxim capacitatea mașinilor RT-qPCR existente (care pot funcționa inclusiv 24/24) prin separarea procesului de extracție ARN, care este mare consumator de timp, și distribuirea acestui proces la multe laboratoare medicale (care oricum nu au ce lucra in această perioadă).

![Alt text](/propunere-1.svg "a title")

Astfel putem crește semnificativ (posibil de 10 ori) numărul de teste efectuate fără a necesita achiziția de echipamente RT-qPCR suplimentare.

## Contraargumente

Principalul contraargument pe care îl anticipăm este că nu vor putea fi asigurate condițiile de siguranță biologică în laboratoarele private din țara. Noi credem că acest argument nu este valid, întrucât biologii/biochimiștii din laboratoare pot fi echipați cu același tip de costume de protecție ca și doctorii care recoltează probele.
Dacă se lucrează cu echipament de protecție, este suficient.

## Cine suntem

- **Alexandru Sălceanu**, biochimist și cercetător în domeniul detecției virale la Institutul National de cercetare în Microtehnologie.
- **Larisa Gogianu**, biolog și cercetător în domeniul detecției virale la Institutul National de cercetare în Microtehnologie.
- **Gabriela Turcanu**, biolog specializarea Microbiologie, asistent de cercetare în domeniul Microbiologiei.
- **Marius Băisan**, CTO @ Softbinator Technologies cu peste 7 ani de experiență în dezvoltarea de sisteme tehnologice medicale
- **Alex Oprișan**, fondator Lansator Pentru Viitor - lansator.ro

## Ce dorim

Dorim să facem cunoscute ideile de mai sus pentru a trece cu toții mai repede și mai bine peste pericolul adus de COVID-19 în România.

## Cum poți contribui și tu

1. Răspândește mesajul. Mesajul trebuie să ajungă la cei care iau decizii privitoare la gestionarea COVID-19 în România.
2. Sugerează moduri de a îmbunătăți această propunere.
3. Spune-ne (cu argumente) dacă propunerea noastră este greșită.

## Citește și:

[O altă metodă propusă de noi pentru eficientizarea testării](/testare-colectiva)
