
## Cine suntem

- **Alexandru Sălceanu**, biochimist și cercetător în domeniul detecției virale la Institutul National de cercetare în Microtehnologie.
- **Larisa Gogianu**, biolog și cercetător în domeniul detecției virale la Institutul National de cercetare în Microtehnologie.
- **Gabriela Turcanu**, biolog specializarea Microbiologie, asistent de cercetare în domeniul Microbiologiei.
- **Marius Băisan**, CTO @ Softbinator Technologies cu peste 7 ani de experiență în dezvoltarea de sisteme tehnologice medicale
- **Alex Oprișan**, fondator Lansator Pentru Viitor - lansator.ro
