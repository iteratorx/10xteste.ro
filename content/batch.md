
# Testarea colectivă pentru creșterea semnificativă a numărului de persoane testate SARS-COV2 în România

## Expunere

Testarea pacientilor pentru depistarea infectiei cu virusul SARS-COV2 presupune 3 etape:

1. Recoltarea probelor biologice
2. Extracția ARN din probele recoltate
3. Testarea ARN prin proces RT-qPCR

Recoltarea probelor biologice (etapa 1) are loc în diferite centre de recoltare, de unde sunt trimise la laboratoarele care dețin echipamente RT-qPCR.

![Testarea în prezent](/descriere-1.svg "Testarea COVID-19 în prezent")

### Testarea  în România

În prezent, în România, depistarea SARS-COV2 se bazează pe efectuarea de teste individuale pentru fiecare persoană, dar abordarea aceasta nu este cea mai eficientă în circumstanțele actuale. Iată de ce:

- în România se fac în prezent aproximativ 5000 de teste SARS-COV2 în fiecare zi
- din acestea aproximativ 500 sunt pozitive. Deci statistic aproximativ 10% din cei testati sunt pozitivi.
- asta inseamnă că aproximativ 90% din testele efectuate nu testează oameni infectați.
- practic folosim 10 teste pentru a depista 1 caz de infecție cu SARS-COV2

### Metoda testării colective

Prezentăm mai jos o metodă de testare colectivă a populației, care crește semnificativ numaărul de persoane testate fără a necesita resurse suplimentare proporționale.

Testarea colectivă presupune alegerea unor loturi de N persoane ale căror probe vor fi procesate împreună, într-o singură extracție ARN (etapa 2) și o singură reacție RT-qPCR (etapa 3).
Dacă rezultatul testului este negativ înseamnă că nicio persoană din lotul respectiv nu este infectată.

Dacă rezultatul testului este pozitiv înseamnă că cel puțin o persoană din grupul testat este pozitivă, deci infectată. În această situație există două variante:

1. se face testarea individuală pentru fiecare persoană din grup ( o extracție ARN si o reacție RT-qPCR pentru fiecare individ).
2. dacă oamenii din grup au fost în contact apropiat (de exemplu sunt membri ai aceleiași familii), ar putea fi considerați toți infectați și plasați în carantină sau izolare.

### Concluzii

Această metodă poate spori considerabil capacitatea de testare a populației, cu resurse limitate, costuri mici, intr-o perioadă de timp mică.
De exemplu, cu împărțirea în grupuri de câte 10 persoane, o populație de 20.000.000 de persoane (din care aproximativ 1% infectate) poate fi testată folosind doar aproximativ 3.600.000 de teste. Adică de aproximativ de 5 ori mai puține teste decăt oameni testati.

Metoda testării colective nu elimină complet testarea individuală, care este necesară în cazul pacienților cu simptome, ci o complementează, fiind foarte potrivită pentru testarea pe scară largă a populației ce nu prezintă simptome (cum se propusese de exemplu testarea în București).

Testarea colectivă este foarte potrivită pentru testarea eficientă, preventivă a grupurilor de persoane care împart același mediu (membri ai aceleiași familii, colegi de muncă, vecini apropiați).

Această idee a fost prezentată anterior de colegul nostru Alexandru Sălceanu într-un interviu realizat de jurnalistul Lucian Mîndruță.

## Cine suntem

- **Alexandru Sălceanu**, biochimist și cercetător în domeniul detecției virale la Institutul National de cercetare în Microtehnologie.
- **Larisa Gogianu**, biolog și cercetător în domeniul detecției virale la Institutul National de cercetare în Microtehnologie.
- **Gabriela Turcanu**, biolog specializarea Microbiologie, asistent de cercetare în domeniul Microbiologiei.
- **Marius Băisan**, CTO @ Softbinator Technologies cu peste 7 ani de experiență în dezvoltarea de sisteme tehnologice medicale
- **Alex Oprișan**, fondator Lansator Pentru Viitor - lansator.ro

## Ce dorim

Dorim să facem cunoscute ideile de mai sus pentru a trece cu toții mai repede și mai bine peste pericolul adus de COVID-19 în România.

## Cum poți contribui și tu

1. Răspândește mesajul. Mesajul trebuie să ajungă la cei care iau decizii privitoare la gestionarea COVID-19 în România.
2. Sugerează moduri de a îmbunătăți această propunere.
3. Spune-ne (cu argumente) dacă propunerea noastră este greșită.

## Citește și:

[Prima metodă propusă de noi pentru eficientizarea testării](/)
