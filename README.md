# 10xTeste.ro

This site is built with Gridsome 

### 1. Install Gridsome CLI tool if you don't have

`npm install --global @gridsome/cli`

### 2. Build and deploy

1. `yarn`
2. edit `urls` array in `scraper/index.js` (add new urls to scrap)
3. `yarn scrap` to scrap statistics form mai.gov.ro
4. optional `yarn develop` to play around
5. `yarn build` to build html files
6. `yarn deploy` to deploy to firebase